// ==UserScript==
// @name         Jira Cloud next-gen project
// @namespace    http://econea.cz/jira-cloud-next-gen
// @version      0.2
// @description  Jira enhancements
// @author       Martin Junger
// @updateUrl    https://bitbucket.org/econea/user-js-jira-cloud/raw/master/jira-cloud-next-gen.user.js
//
// @match        https://*.atlassian.net/jira/software/projects/*/boards/*/backlog*
// @grant        GM_xmlhttpRequest
// @grant        GM_addStyle
//
//   jQuery
// @require      http://code.jquery.com/jquery-latest.min.js
//
// @run-at       document-end
// ==/UserScript==

(function() {
    'use strict';

    GM_addStyle(`
        .customStatus, .customDpt, .customLabel {
            padding-left: 8px;
            box-sizing: border-box;
            display: inline-block;
            font-size: 11px;
            font-weight: 700;
            line-height: 1;
            max-width: 100%;
            text-transform: uppercase;
            vertical-align: baseline;
            border-radius: 3px;
            padding: 4px 3px 3px;
        }
        .customStatus {
            background-color: lightblue;
            color: navy;
        }
        .customDpt {
            background-color: antiquewhite;
            color: darkred;
        }
        .customLabel {
            background-color: lightgray;
            color: darkviolet;
        }
    `);

    // 0 = off; 1 = standard; 2 = 1 + json;
    var debug = 0;

    if (debug) console.log('tickets', $('div[data-rbd-droppable-id="20"] > div, div[data-rbd-droppable-id="BACKLOG"] > div'));
    $('div[data-rbd-droppable-id]')
        .filter(function() {
            var id = $(this).data('rbd-droppable-id');
            // number of sprint or 'BACKLOG'
            return !isNaN(id) || id === 'BACKLOG';
        })
        .find('> div')
        .each(function(index, el) {

            // check ticket
            if ($(el).find('a').length === 0) {
                return;
            }
            // get ticket
            if (debug) console.log('href', $(el).find('a').attr('href'));
            var id = $(el).find('a').attr('href').replace('/browse/', '');
            if (debug) console.log('id', id);

            // get ticket info
            var apiUrl = 'https://' + document.domain + '/rest/api/3/issue/' + id;
            if (debug) console.log('apiUrl', apiUrl);

            GM_xmlhttpRequest({
                method: 'GET',
                url: apiUrl,
                onload: function(response) {
                    if (response.status == 200) {
                        if (debug >= 2) console.log('responseText', response.responseText);
                        var responseObject = JSON.parse(response.responseText);

                        // get status
                        var status = responseObject.fields.status.name;
                        if (debug) console.log('status', status);

                        // get labels
                        var labels = responseObject.fields.labels;
                        if (debug) console.log('labels', labels);
                        labels = labels.map(function(label) {
                            return label.slice(0, 7);
                        });
                        if (debug) console.log('labels filtered', labels);

                        // get departments
                        if (debug) console.log('dpts', responseObject.fields.customfield_10032);
                        var dpts = responseObject.fields.customfield_10032 || [];
                        dpts = dpts.map(function(dpt) {
                            return dpt.value.slice(0, 7);
                        });
                        if (debug) console.log('dpts filtered', dpts);

                        window.setTimeout(function(id, status, dpts, labels) {
                            if (debug) console.log('timeout', id, labels);
                            var divTitle = $('a[href="/browse/'+id+'"]:eq(1)').parent().parent().find('div:nth-child(3):first');
                            if (debug) console.log('divTitle', divTitle);

                            var divStatus = $('<div class="customStatus">'+status+'</div>');
                            if (debug) console.log('divStatus', divStatus);
                            divStatus.insertBefore(divTitle);

                            if (dpts && dpts.length) {
                                var divDpts = $('<div class="customDpt">'+dpts.join('</div><div class="customDpt">')+'</div>');
                                if (debug) console.log('divDpts', divDpts);
                                divDpts.insertBefore(divTitle);
                            }

                            if (labels && labels.length) {
                                var divLabels = $('<div class="customLabel">'+labels.join('</div><div class="customLabel">')+'</div>');
                                if (debug) console.log('divLabels', divLabels);
                                divLabels.insertBefore(divTitle);
                            }
                        }, 5000, id, status, dpts, labels);
                    }
                }
            });

        });
})();