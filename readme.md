# Jira Cloud next-gen

Uzivatelsky Javascript, ktery upravuje chovani aplikace Jira.

## Upravy

### Project Backlog

Zobrazi jako tagy:

  - Status
  - Oddeleni
  - Stitky

![Jira Backlog - tags](doc/jira-backlog-tags.png)

## Pouziti:

1) Nainstalovat rozsireni do prohlizece Tampermonkey (nebo podobne)

2a) V prohlizeci prejit na RAW verzi souboru s koncovkou `.user.js`, mel by se nabidnout import do Tampermonkey (dalsi aktualizace budou automaticke)

2b) Importovat lokalni soubor s koncovkou `.user.js` do Tampermonkey (dalsi aktualizace musi zajistit uzivatel rucne: Git + imprort)

3) Prejit do aplikace Jira, kde budou videt upravy
